'''
versionutil

Convenience functions for comparing Python system version
'''
from .version import compare_python, PYTHON, PythonVersion

__title__ = 'versionutil'
__version__ = '0.1.3'
__all__ = ('compare_python', 'PythonVersion', 'PYTHON')
__author__ = 'Johan Nestaas <johannestaas@gmail.com>'
__license__ = 'GPLv3+'
__copyright__ = 'Copyright 2016 Johan Nestaas'


def main():
    pass


if __name__ == '__main__':
    main()
