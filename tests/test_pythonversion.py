import os
import sys
from random import randint as orig_randint
from collections import namedtuple

rootdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path = [rootdir] + sys.path
from versionutil import PythonVersion

Mock = namedtuple('MockVersion',
                           'major minor micro releaselevel serial')


def test_python_version():
    for major in range(5):
        for minor in range(20):
            for micro in range(20):
                key = 'v{0}{1:02}{2:02}'.format(major, minor, micro)
                mocked = Mock(major, minor, micro, 'final', 999)
                globals()[key] = PythonVersion(sysv=mocked)
    assert v20608 < 2.7
    assert PythonVersion(sysv=(2, 6, 8)) < 2.7
    assert v20608 == 2.6
    assert v20608 > 2.5
    assert v20608 != 2.7
    assert v20608 < 3
    assert v20608 == '2.6.8'
    assert v20608 < '2.7'
    assert v20608 == '2.6'
    assert v20608 > '2.5'
    assert v20608 != '2.7'
    assert v20608 < '3'

    assert v20711 == '2.7.11'
    assert v20711 == '2.7'
    assert v20711 == '2'
    assert v20711 == 2.7
    assert v20711 == 2
    for i in range(3, 20):
        assert v20711 < i
    for i in range(10):
        assert v20711 < 3.0 + (i / 10.0)
    for i in range(11, 21):
        assert v20711 < 3.0 + (i / 100.0)
    assert v20711 < 3
    assert v20711 < 3.2
    assert v20711 < 3.4
    assert v20711 < 3.5
    assert v20711 > 2.6
    assert v20711 != 2.6
    assert v20711 != 3

    assert v30000 == 3
    assert v30000 == 3.0
    assert v30000 == '3'
    assert v30000 == '3.0'
    assert v30000 == '3.0.0'
    assert v30000 >= '3.0.0'
    assert v30000 >= '2.9.11'
    assert v30000 > '2.9.11'
    assert v30000 > '2.8.111'
    assert v30000 > '2.99.99'
    assert v30000 < '3.1.0'
    assert v30000 < '3.0.1'
    assert v30000 < '3.1.1'
    assert v30202 == '3.2.2'
    assert v30202 == 3.2
    assert v30202 < '3.4.4'
    assert v30202 <= '3.4.4'
    assert v30202 >= '3.2.2'
    assert v30202 != '3.2.0'
    assert v30202 > 2
    assert v30202 < 4
    assert v30202 >= 2
    assert v30202 <= 4
    assert v30202 > '2.0.0'
    assert v30202 > '2.0.1'
    assert v30202 >= '2.0.0'
    assert v30404 == '3.4.4'
    assert v30404 == '3.4'
    assert v30404 == 3.4
    assert v30404 == 3
    assert v30404 >= '3.4.4'
    assert v30404 >= '3.4'
    assert v30404 >= 3.4
    assert v30404 >= 3
    assert v30500 == '3.5.0'
    assert v30500 == '3.5'
    assert v30500 == '3'
    assert v30500 == 3.5
    assert v30500 == 3
    assert v20711 < 3.0
    assert v20711 < 3
    assert v20711 < '3.2.2'
    assert v20711 > 2.6
    assert v20711 > '2.7.9'
    assert v20711 >= '2.7.11'
    assert v20711 >= '2.7.9'
