import os
import sys
from random import randint as orig_randint
from collections import namedtuple

rootdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path = [rootdir] + sys.path
from versionutil import compare_python

MockVersionNT = namedtuple('MockVersion',
                           'major minor micro releaselevel serial')


def randint(*args):
    if len(args) == 1:
        return orig_randint(0, args[0])
    return orig_randint(*args)


def mm2float(major, minor):
    if minor < 10:
        # Turns 3, 1 into 3.0 + 0.1
        return round(float(major) + (float(minor) / 10), 1)
    elif minor == 10:
        # Cant use a float like 3.10 because it'd come out same as 3.1
        return None
    else:
        # 3, 11 turns into 3.0 + 0.11 because of divide 100
        return round(float(major) + (float(minor) / 100), 2)


def MockVersion(mmm):
    ''' Always makes releaselevel and serial 'final' and 1 '''
    return MockVersionNT(mmm[0], mmm[1], mmm[2], 'final', 1)


def mock_sys(*args):
    '''
    Turns mock_sys(3, 2) into something like:
    MockVersionNT(major=3, minor=2, micro=4, ...)
    '''
    if len(args) == 0:
        return MockVersion((2, 7, 11))
    if len(args) == 1:
        if args[0] == 2:
            # 2.7.11
            return MockVersion(args + (7, 11))
        else:
            # 3.4.4
            return MockVersion(args + (4, 4))
    if len(args) == 2:
        if args[0] == 2:
            # 2.x.9
            return MockVersion(args + (9,))
        else:
            # 3.x.4
            return MockVersion(args + (4,))
    return args[:3]


def assert_eq(arg, sysargs):
    assert compare_python(arg, sysv=mock_sys(*sysargs), comp='==')
    assert compare_python(arg, sysv=mock_sys(*sysargs), comp='>=')
    assert compare_python(arg, sysv=mock_sys(*sysargs), comp='<=')
    assert not compare_python(arg, sysv=mock_sys(*sysargs), comp='!=')
    assert not compare_python(arg, sysv=mock_sys(*sysargs), comp='<')
    assert not compare_python(arg, sysv=mock_sys(*sysargs), comp='>')
    if arg.count('.') == 1:
        s1, s2 = arg.split('.')
        if int(s2) > 9:
            return
        farg = round(float(arg), 2)
        assert compare_python(farg, sysv=mock_sys(*sysargs), comp='==')
        assert compare_python(farg, sysv=mock_sys(*sysargs), comp='>=')
        assert compare_python(farg, sysv=mock_sys(*sysargs), comp='<=')
        assert not compare_python(farg, sysv=mock_sys(*sysargs), comp='!=')
        assert not compare_python(farg, sysv=mock_sys(*sysargs), comp='<')
        assert not compare_python(farg, sysv=mock_sys(*sysargs), comp='>')


def assert_neq(arg, sysargs):
    assert compare_python(arg, sysv=mock_sys(*sysargs), comp='!=')
    if arg.count('.') == 1:
        s1, s2 = arg.split('.')
        if int(s2) > 9:
            return
        assert compare_python(float(arg), sysv=mock_sys(*sysargs), comp='!=')


def assert_gt(arg, sysargs):
    assert compare_python(arg, sysv=mock_sys(*sysargs), comp='>')
    assert compare_python(arg, sysv=mock_sys(*sysargs), comp='>=')
    assert not compare_python(arg, sysv=mock_sys(*sysargs), comp='==')
    assert not compare_python(arg, sysv=mock_sys(*sysargs), comp='<')
    if arg.count('.') == 1:
        s1, s2 = arg.split('.')
        if int(s2) > 9:
            return
        farg = round(float(arg), 2)
        assert compare_python(farg, sysv=mock_sys(*sysargs), comp='>')
        assert compare_python(farg, sysv=mock_sys(*sysargs), comp='>=')
        assert not compare_python(farg, sysv=mock_sys(*sysargs), comp='==')
        assert not compare_python(farg, sysv=mock_sys(*sysargs), comp='<')


def assert_gte(arg, sysargs):
    assert (
        compare_python(arg, sysv=mock_sys(*sysargs), comp='>') or
        compare_python(arg, sysv=mock_sys(*sysargs), comp='==')
    )
    assert compare_python(arg, sysv=mock_sys(*sysargs), comp='>=')
    assert not compare_python(arg, sysv=mock_sys(*sysargs), comp='<')
    if arg.count('.') == 1:
        s1, s2 = arg.split('.')
        if int(s2) > 9:
            return
        farg = round(float(arg), 2)
        assert (
            compare_python(farg, sysv=mock_sys(*sysargs), comp='>') or
            compare_python(farg, sysv=mock_sys(*sysargs), comp='==')
        )
        assert compare_python(farg, sysv=mock_sys(*sysargs), comp='>=')
        assert not compare_python(farg, sysv=mock_sys(*sysargs), comp='<')


def assert_lt(arg, sysargs):
    assert compare_python(arg, sysv=mock_sys(*sysargs), comp='<')
    assert compare_python(arg, sysv=mock_sys(*sysargs), comp='<=')
    assert not compare_python(arg, sysv=mock_sys(*sysargs), comp='==')
    assert not compare_python(arg, sysv=mock_sys(*sysargs), comp='>')
    if arg.count('.') == 1:
        s1, s2 = arg.split('.')
        if int(s2) > 9:
            return
        farg = round(float(arg), 2)
        assert compare_python(farg, sysv=mock_sys(*sysargs), comp='<')
        assert compare_python(farg, sysv=mock_sys(*sysargs), comp='<=')
        assert not compare_python(farg, sysv=mock_sys(*sysargs), comp='==')
        assert not compare_python(farg, sysv=mock_sys(*sysargs), comp='>')


def assert_lte(arg, sysargs):
    assert (
        compare_python(arg, sysv=mock_sys(*sysargs), comp='<') or
        compare_python(arg, sysv=mock_sys(*sysargs), comp='==')
    )
    assert compare_python(arg, sysv=mock_sys(*sysargs), comp='<=')
    assert not compare_python(arg, sysv=mock_sys(*sysargs), comp='>')
    if arg.count('.') == 1:
        s1, s2 = arg.split('.')
        if int(s2) > 9:
            return
        farg = round(float(arg), 2)
        assert (
            compare_python(farg, sysv=mock_sys(*sysargs), comp='<') or
            compare_python(farg, sysv=mock_sys(*sysargs), comp='==')
        )
        assert compare_python(farg, sysv=mock_sys(*sysargs), comp='<=')
        assert not compare_python(farg, sysv=mock_sys(*sysargs), comp='>')


def test_check_major():
    assert_eq('2', (2, 7, 11))
    assert_gt('2', (3, 2, 2))
    assert_gt('2', (3, 4, 4))
    assert_lt('3', (2, 7, 9))
    assert_lt('3', (2, 7, 11))
    assert_neq('2', (3, 5, 0))
    assert_neq('3', (2, 7, 11))
    for vm in range(0, 20):
        for minor in range(20):
            for micro in range(20):
                assert_eq(str(vm), (vm, minor, micro))
                assert_gt(str(vm), (vm + 1, minor, micro))
                if vm > 0:
                    assert_lt(str(vm), (vm - 1, minor, micro))
                assert_gte(str(vm), (vm, minor, micro))
                assert_lte(str(vm), (vm, minor, micro))
                assert_neq(str(vm), (vm + 1, minor, micro))
                if vm > 0:
                    assert_neq(str(vm), (vm - 1, minor, micro))


def test_check_minor():
    assert_eq('2.7', (2, 7, 11))
    assert_eq('2.6', (2, 6, 8))
    assert_eq('3.0', (3, 0, 0))
    assert_eq('3.0', (3, 0, 1))
    assert_eq('3.2', (3, 2, 0))
    assert_eq('3.2', (3, 2, 2))
    assert_eq('3.3', (3, 3, 0))
    assert_eq('3.3', (3, 3, 3))
    assert_eq('3.4', (3, 4, 0))
    assert_eq('3.4', (3, 4, 4))
    assert_eq('3.5', (3, 5, 0))
    assert_eq('3.5', (3, 5, 4))
    for major in range(10):
        for minor in range(20):
            assert_eq('{major}.{minor}'.format(
                major=major, minor=minor), (major, minor, randint(0, 20))
            )
            assert_neq('{major}.{minor}'.format(
                major=major, minor=minor + 1), (major, minor, randint(0, 20))
            )
            assert_neq('{major}.{minor}'.format(
                major=major + 1, minor=minor), (major, minor, randint(0, 20))
            )
            assert_neq('{major}.{minor}'.format(
                major=major, minor=minor), (major + 1, minor, randint(0, 20))
            )
            assert_neq('{major}.{minor}'.format(
                major=major, minor=minor), (major, minor + 1, randint(0, 20))
            )
            assert_lt('{major}.{minor}'.format(
                major=major + 1, minor=minor), (major, minor, randint(0, 20))
            )
            assert_lte('{major}.{minor}'.format(
                major=major + 1, minor=minor), (major, minor, randint(0, 20))
            )
            assert_lt('{major}.{minor}'.format(
                major=major, minor=minor + 1), (major, minor, randint(0, 20))
            )
            assert_lte('{major}.{minor}'.format(
                major=major, minor=minor + 1), (major, minor, randint(0, 20))
            )
            assert_gt('{major}.{minor}'.format(
                major=major, minor=minor), (major + 1, minor, randint(0, 20))
            )
            assert_gte('{major}.{minor}'.format(
                major=major, minor=minor), (major + 1, minor, randint(0, 20))
            )
            assert_gt('{major}.{minor}'.format(
                major=major, minor=minor), (major, minor + 1, randint(0, 20))
            )
            assert_gte('{major}.{minor}'.format(
                major=major, minor=minor), (major, minor + 1, randint(0, 20))
            )


def test_check_micro():
    assert_eq('2.5.4', (2, 5, 4))
    assert_eq('2.6.8', (2, 6, 8))
    assert_eq('2.7.11', (2, 7, 11))
    assert_eq('3.0.0', (3, 0, 0))
    assert_eq('3.0.1', (3, 0, 1))
    assert_eq('3.2.0', (3, 2, 0))
    assert_eq('3.2.2', (3, 2, 2))
    assert_eq('3.3.0', (3, 3, 0))
    assert_eq('3.3.3', (3, 3, 3))
    assert_eq('3.4.0', (3, 4, 0))
    assert_eq('3.4.4', (3, 4, 4))
    assert_eq('3.5.0', (3, 5, 0))
    assert_eq('3.5.4', (3, 5, 4))
    assert_neq('2.5.5', (2, 5, 4))
    assert_neq('2.6.7', (2, 6, 8))
    assert_neq('2.7.10', (2, 7, 11))
    assert_neq('3.0.1', (3, 0, 0))
    assert_neq('3.1.1', (3, 0, 1))
    assert_neq('3.2.2', (3, 2, 0))
    assert_neq('3.2.0', (3, 2, 2))
    assert_neq('3.3.3', (3, 3, 0))
    assert_neq('3.3.0', (3, 3, 3))
    assert_neq('3.4.4', (3, 4, 0))
    assert_neq('3.4.0', (3, 4, 4))
    assert_neq('3.5.4', (3, 5, 0))
    assert_neq('3.5.0', (3, 5, 4))

    def mstr(major, minor, micro):
        return '{major}.{minor}.{micro}'.format(major=major, minor=minor,
                                                micro=micro)
    for major in range(10):
        for minor in range(20):
            for micro in range(20):
                mmm = (major, minor, micro)
                m1mm = (major + 1, minor, micro)
                mm1m = (major, minor + 1, micro)
                mmm1 = (major, minor, micro + 1)
                smmm = mstr(major, minor, micro)
                sm1mm = mstr(major + 1, minor, micro)
                smm1m = mstr(major, minor + 1, micro)
                smmm1 = mstr(major, minor, micro + 1)
                assert_eq(smmm, mmm)
                assert_eq(sm1mm, m1mm)
                assert_eq(smm1m, mm1m)
                assert_eq(smmm1, mmm1)
                assert_gte(smmm, mmm)
                assert_gte(sm1mm, m1mm)
                assert_gte(smm1m, mm1m)
                assert_gte(smmm1, mmm1)
                assert_lte(smmm, mmm)
                assert_lte(sm1mm, m1mm)
                assert_lte(smm1m, mm1m)
                assert_lte(smmm1, mmm1)
                assert_gt(smmm, m1mm)
                assert_gt(smmm, mm1m)
                assert_gt(smmm, mmm1)
                assert_gt(smm1m, m1mm)
                assert_gt(smmm1, m1mm)
                assert_gt(smmm1, mm1m)
                assert_lt(sm1mm, mmm)
                assert_lt(smm1m, mmm)
                assert_lt(smmm1, mmm)
                assert_lt(smm1m, mmm1)
                assert_lt(sm1mm, mm1m)
                assert_neq(smmm, m1mm)
                assert_neq(smmm, mm1m)
                assert_neq(smmm, mmm1)
                for i in range(10):
                    assert_gt(smmm, (major + 1, randint(20), randint(20)))
                    assert_gte(smmm, (major + 1, randint(20), randint(20)))
                for i in range(10):
                    assert_gt(smmm, (major, minor + randint(1, 20),
                                     randint(20)))
                    assert_gte(smmm, (major, minor + randint(1, 20),
                                      randint(20)))
                for i in range(micro + 1, 100):
                    assert_gt(smmm, (major, minor, i))
                    assert_gte(smmm, (major, minor, i))
                if major > 0:
                    for i in range(10):
                        assert_lt(smmm, (major - 1, randint(20), randint(20)))
                if minor > 0:
                    for i in range(10):
                        assert_lt(smmm, (major, minor - 1, randint(20)))
